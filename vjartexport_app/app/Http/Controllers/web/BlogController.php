<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index()
    {
        return view('frontend.pages.blog.index');
    }
    public function show( $blog)
    {
        return view('frontend.pages.blog.show');
    }
}
