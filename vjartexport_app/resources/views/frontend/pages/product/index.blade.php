@extends('frontend.layouts.app')

@section('site_title','Home')

@section('content')
<!-- Head Section -->
<section class="page-section bg-dark-alfa-50 parallax-3" data-background="{{url('web/image/1021/03.jpg')}}">
        <div class="relative container align-left">
            <div class="row">
                <div class="col-md-8">
                    <h1 class="hs-line-11 font-alt mb-20 mb-xs-0">Portfolio</h1>
                    <div class="hs-line-4 font-alt">
                        An eye for detail makes our works excellent 
                    </div>
                </div>

                <div class="col-md-4 mt-30">
                    <div class="mod-breadcrumbs font-alt align-right">
                        <a href="../index.html">Home</a>
                                <span>&nbsp;/ &nbsp;</span><span>Products</span>
                    </div>

                </div>
            </div>

        </div>
    </section>
    <!-- End Head Section -->

    <!-- Portfolio Section -->
    <section class="page-section pb-0">
        <div class="container relative">
                <!-- Works Filter -->
                <!-- <div class="works-filter font-alt align-center">
                    <a href="#" class="filter active" data-filter="*">All works</a>
                        <a href="#vintage-industrial" class="filter" data-filter=".vintage-industrial">Vintage Industrial</a>
                        <a href="#accent-furniture" class="filter" data-filter=".accent-furniture">Accent Furniture</a>
                        <a href="#mordern-contemprary" class="filter" data-filter=".mordern-contemprary">Mordern Contemprary</a>
                        <a href="#straight-line" class="filter" data-filter=".straight-line">Straight Line</a>
                        <a href="#home-decor" class="filter" data-filter=".home-decor">Home Decor</a>
                </div> -->
                <!-- End Works Filter -->
                <!-- Works Grid -->
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="border: #000 solid 0px;">
                            <h4 class="title-sidebar">Product Categories</h4>
                            <div class="right_blog_list">
                                <ul class="product-dropdown">
                                    <li>
                                        <a href="">Vintage Industrial</a>
                                        <i class="bi bi-chevron-right icon"></i>
                                        <ul class="product-dropdown-list"></ul>
                                    </li>
                                    <li>
                                        <a href="">Accent Furniture</a>
                                        <i class="bi bi-chevron-right icon"></i>
                                        <ul class="product-dropdown-list"></ul>
                                    </li>
                                    <li>
                                        <a href="">Modern Contemprary</a>
                                        <i class="bi bi-chevron-right icon"></i>
                                        <ul class="product-dropdown-list"></ul>
                                    </li>
                                    <li>
                                        <a href="">Straight Line</a>
                                        <i class="bi bi-chevron-right icon"></i>
                                        <ul class="product-dropdown-list"></ul>
                                    </li>
                                    <li>
                                        <a href="">Home Decor</a>
                                        <i class="bi bi-chevron-right icon"></i>
                                        <ul class="product-dropdown-list"></ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12" style="border: #000 solid 0px;">
                            <h4 class="middle-title-sidebar">Product Categories</h4>
                            <div>
                                <div class="row">
                                    @for($i = 1; $i <= 16; $i++)
                                    <div class="col-lg-4 col-md-4 col-sm-4 p_Categories">
                                        <a href="{{route('product.show',1)}}">
                                            <img src="{{url('web/image/download.jpg')}}" alt="">
                                        </a>
                                        <a href="{{route('product.show',1)}}">
                                            <div class="p_Categories_text">Distressed Blue Solid Wood Top Iron Park Seating Bench</div> 
                                        </a>
                                        <button type="submit" class="add_btn">Enquiry Now<i class="bi bi-cart4"></i></button>
                                    </div>
                                    @endfor
                                
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- End Works Grid -->
        </div>
    </section>
    <!-- End Portfolio Section -->
    <!-- Call Action Section -->
    <section class="small-section bg-dark">
        <div class="container relative">

            <div class="align-center">
                <h3 class="banner-heading font-alt">Like Our Creative Works?</h3>
                <div>
                    <a href="{{url('contact-us')}}" class="btn btn-mod btn-w btn-medium btn-round">Start Project</a>
                </div>
            </div>

        </div>
    </section>
    <!-- End Call Action Section -->

@endsection