<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/run-cmd/{cmd}', function ($cmd) {
    return \Artisan::call($cmd);
});


Route::group(['namespace'=>'App\Http\Controllers'],function(){
    #Backend Routes
    Route::group(['prefix'=>'admin-control'],function()
    {
        Route::get('/login', 'AuthController@showLogin')->name('login');
        Route::post('/login', 'AuthController@login');
        Route::get('/logout', 'AuthController@logout')->name('logout');

        Route::group(['as'=>'admin.','middlewear' => 'auth'], function () {
            Route::get('/','DashboardController@dashboard')->name('dashboard');
    
            // Route::resources([
            //     'post/{post_type:slug}' => 'PostController',
            // ]);
            Route::get('{post_type:slug}', 'PostController@index')->name('post.index');
            Route::post('{post_type:slug}', 'PostController@store')->name('post.store');
            Route::get('{post_type:slug}/create', 'PostController@create')->name('post.create');
        }); 
    });


    #website Routes
    Route::group(['namespace'=>'web'],function() {
        Route::get('/','HomeController@index')->name('home');
        Route::get('about-us','AboutController@about')->name('about');
        Route::get('contact-us','ContactController@contact')->name('contact');
        // Route::get('products','ProductController@product')->name('product');
        Route::resource('product', 'ProductController')->only(['index', 'show']);
        // Route::get('blog','BlogController@blog')->name('blog');
        Route::resource('blog','BlogController')->only(['index','show']);
    });
});
