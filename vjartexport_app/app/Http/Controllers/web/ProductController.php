<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        return view('frontend.pages.product.index');
    }
    public function show( $product)
    {
        return view('frontend.pages.product.show');
    }
}
