<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use Illuminate\Http\Request;
use App\Models\Site;
use App\Models\PostType;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(Request $request)
    {
        $domain = $request->getHost();
        $site = Site::where('domain', $domain)->firstOrFail();
        $post_types = PostType::where('site_id', $site->id)->get();

        \View::share(compact('site', 'post_types'));
    }
}
