@extends('backend.layouts.inner')

@section('site_title','Dashboard')

@section('content')

<div class="container py-4">
        <div class="bg-white">
            <h1>Page</h1>
            <h2>DashBoard/Services/Create</h2>
        </div>
        <form action="{{route('admin.post.store',[$post_type->slug])}}" method="post">
            @csrf
        <div class="row pt-5 bg-white">
            <div class="col-8  text-dark">
                <div class="row">
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="exampleFormControlInput1" class="form-label">Title</label>
                            <input type="text" class="form-control" name="title" id="exampleFormControlInput1" placeholder="title">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="exampleFormControlInput1" class="form-label">Slug</label>
                            <input type="text" class="form-control" name="slug" id="exampleFormControlInput1" placeholder="slug">
                        </div>
                    </div>
                </div>
                
                
                <div class="mb-3">
                    <label for="exampleFormControlTextarea1" class="form-label">Short Description</label>
                    <textarea class="form-control" name="excerpt" id="exampleFormControlTextarea1" rows="2"></textarea>
                </div>
                <div class="mb-3">
                    <label for="exampleFormControlTextarea1" class="form-label">Description</label>
                    <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>

            </div>
                <div class="col-4 ">
                    <div class="card" style="width: 18rem;">
                        <img src="{{url('image/download.png')}}" class="card-img-top" alt="..." name="image">
                    </div>
                
                    <button class="btn btn-primary mt-3">Choose image</button>
                </div>
                <div class="row">
                    <div class="col-1">
                        <button type="submit" class="btn btn-primary">Save</button>

                    </div>
                    <div class="col-1">
                        <button type="button" class="btn btn-primary">Reset</button>

                    </div>
                </div>
        </div>
        </form>
    </div>

@endsection