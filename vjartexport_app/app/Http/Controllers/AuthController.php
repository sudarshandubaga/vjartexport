<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class AuthController extends Controller
{
    public function showLogin()
    {
        return view('backend.pages.login');
    }
    public function login(Request $request)
    {
        $request->validate([
            'email' =>  'required|email',
            'password'  =>  'required|string|min:8|max:16'
        ]);

        $check = Auth::attempt( $request->only(['email','password']), $request->remeber);

        if($check) {
            return redirect( route('admin.dashboard') );
        }   else {
            return redirect()->back()->withErrors(['msg' => 'Login failed! Email or Password is incorrect.']);
        }
    }

    public function logout()
    {
        Auth::logout();

        return redirect( route('login'));
    }
}
