@extends('frontend.layouts.app')

@Section('site_title','Home')

@section('content')

<!-- Head section -->
<section class="page-section bg-dark-alfa-50 parallax-3" data-background="{{url('web/image/1004/section-bg-10.jpg')}}">
        <div class="relative container align-left">
            <div class="row">
                <div class="col-md-8">
                    <h1 class="hs-line-11 font-alt mb-20 mb-xs-0">Blog</h1>
                    <div class="hs-line-4 font-alt">
                        Extraordinary art team &amp; creative minimalism lovers at VJ
                    </div>
                </div>

                <div class="col-md-4 mt-30">
                    <div class="mod-breadcrumbs font-alt align-right">
                        <a href="{{url('/')}}">Home</a>
                                <span>&nbsp;/ &nbsp;</span><span>Blog</span>
                    </div>

                </div>
            </div>

        </div>
    </section>
<!-- End Head Section  -->

<!-- Blog Section  -->
<section>
    <div class="relative container">
        <div class="row">
            @for($i=1; $i <= 4; $i++)
            <div class="col-lg-9 col-md-9 col-sm-9">
                
                <div class="mt-80">
                    <img src="{{url('web/image/post-1.jpg')}}" alt="">
                </div>
                <h2 class="blog_name_title mb-5">Wooden Dining Table set</h2>
                <div class="blog_content mb-20">
                    <p>
                    Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Suspendisse potenti. Sed egestas, ante et vulputate volutpat, uctus metus libero eu augue.
                    </p>

                    <a href="">continue reading</a>
                </div>
                
            </div>
            @endfor
        </div>
    </div>
</section>
<!-- End Blog Section  -->
@endsection