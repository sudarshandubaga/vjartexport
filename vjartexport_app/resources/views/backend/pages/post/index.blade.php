<!-- @extends('backend.layouts.inner')

<h1>View {{ $post_type->name }}</h1> -->

@extends('backend.layouts.inner')

@section('site_title','Dashboard')

@section('content')

<div class="container">
    <div class="bgindex">
        <h1>Page</h1>
        <h2>DashBoard/Service/Index</h2>
    </div>
    
    <table class="table table-striped py-5">
  <thead>
    <tr>
      <th><input class="form-check-input" type="checkbox" value="" name="sr-no." >Sr .No</th>
      <th >Title</th>
      <th >Image</th>
      <th >Short Description</th>
      <th >Description</th>
    </tr>
  </thead>
  <tbody>
    @foreach($post_types as $index => $post_type)
    <tr>
      <th scope="row"><input class="form-check-input" type="checkbox" name="sr-no."></th>
      <td>{{ $post_type->name}}</td>
      <td>{{ $post_type->image}}</td>
      <td>{{ $post_type->excerpt}}</td>
      <td>{{ $post_type->description}}</td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>

@endsection