<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('site_title' | 'VJ Art Exports')</title>

    <!-- Favicons -->
    <link rel="Shortcut icon" href="{{url('web/image/favicon.png')}}">

    <!-- css -->
    <link rel="stylesheet" href="{{url('web/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('web/css/style.css')}}">
    <link rel="stylesheet" href="{{url('web/css/style-responsive.css')}}">
    <link rel="stylesheet" href="{{url('web/css/animate.min.css')}}">
    <link rel="stylesheet" href="{{url('web/css/vertical-rhythm.min.css')}}">
    <link rel="stylesheet" href="{{url('web/css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{url('web/css/magnific-popup.css')}}">
    <!-- <link rel="stylesheet" href="{{url('web/css/et-line.css')}}"> -->
    
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
</head>
<body class="appear-animate">

    <!-- Page Loader -->
    <div class="page-loader">
        <div class="loader">Loading...</div>
    </div>
    <!-- End Page Loader -->
    <!-- Page Wrap -->
    <div class="page" id="top">
        


<!-- Navigation panel -->
    <nav class="main-nav dark js-stick">

        <div class="container">
            <div class="full-wrapper relative clearfix">
                <!-- Logo ( * your text or image into link tag *) -->
                <div class="nav-logo-wrap local-scroll">
                        <a href="{{url('/')}}" class="logo">
                                <img src="{{url('web/image/1015/vj-logo.png')}}" alt="VJ Logo.png" />
                        </a>
                </div>
                <div class="mobile-nav">
                    <i class="fa fa-bars"></i>
                </div>

                <!-- Main Menu -->
                <div class="inner-nav desktop-nav">
                    <ul class="clearlist">
                                <!-- Item With Sub -->
                                <li>
                                    <a href="{{url('/')}}" class="active">Home</a>
                                </li>
                                <!-- End Item With Sub -->
                                <!-- Item With Sub -->
                                <li>
                                    <a href="{{url('about-us')}}" class="">About Us</a>
                                </li>
                                <!-- End Item With Sub -->
                                <!-- Item With Sub -->
                                <li>
                                    <a href="{{route('product.index')}}" class="">Products</a>
                                </li>
                                <!-- End Item With Sub -->
                                <!-- Item With Sub -->
                                <li>
                                    <a href="{{route('blog.index')}}" class="">Blog</a>
                                </li>
                                <!-- End Item With Sub -->
                                <!-- Item With Sub -->
                                <li>
                                    <a href="{{url('contact-us')}}" class="">Contact Us</a>
                                </li>
                                <!-- End Item With Sub -->
                        <!-- Divider -->
                        <!-- <li><a>&nbsp;</a></li> -->
                        <!-- End Divider -->
                            <!-- Search -->
                            <!-- <li>
                                <a href="#" class="mn-has-sub"><i class="bi bi-search"></i> Search</a>

                                <ul class="mn-sub">

                                    <li>
                                        <div class="mn-wrap">
                                            <form method="get" action="http://vjartexports.com/search-result/" class="form">
                                                <div class="search-wrap">
                                                    <button class="search-button animate" type="submit" title="Start Search">
                                                    <i class="bi bi-search"></i>
                                                    </button>
                                                    <input type="text" name="searchterm" class="form-control search-field" placeholder="Search...">
                                                </div>
                                            </form>
                                        </div>
                                    </li>

                                </ul>

                            </li> -->
                            <!-- End Search -->
                    </ul>
                </div>
                <!-- End Main Menu -->
            </div>
        </div>
    </nav>
@yield('content')
        <!-- Foter -->
    <footer class="page-section dark footer pb-60">
        <div class="container">
          <div class="row">
                <div class="col-lg-3">
                    <!-- Footer Logo -->
                        <div class="local-scroll mb-30 wow fadeInUp" data-wow-duration="1.5s">
                                <a href=" ">
                                        <img class="footer-logo" src="{{url('web/image/1017/vj-logo-footer-1.png')}}" width="78" alt="VJ Logo footer 1.png" />
                                </a>
                        </div>
                        <!-- End Footer Logo -->
                        <!-- Social Links -->
                        <div class="footer-social-links mb-30 mb-xs-60">
                                    <a href="https://www.facebook.com/VJ-Art-Exports-290625985109491/?modal=admin_todo_tour" title="Facebook" target="_blank"><i class="bi bi-facebook"></i></a>
                                    <a href="https://twitter.com/VjExports?lang=en" title="Twitter" target="_blank"><i class="bi bi-twitter"></i></a>
                                    <a href="https://www.linkedin.com/feed/" title="LinkedIn" target="_blank"><i class="bi bi-linkedin"></i></a>
                                    <a href="https://www.pinterest.com/vjartexports/" title="Pinterest" target="_blank"><i class="bi bi-pinterest"></i></a>
                        </div>
                        <!-- End Social Links -->
                        <!-- Footer Text -->
                        <div class="footer-text">

                            <!-- Copyright -->
                            <div class="footer-copy font-alt">
                                <a href="index.html" target="_blank">&#169; VJ Art Exports 2018</a>.
                            </div>
                            <!-- End Copyright -->

                            <div class="footer-made">
                                Made with love for great people.
                            </div>
                        </div>
                        <!-- End Footer Text -->
                </div>                                    
                <div class="col-lg-3 mt-80">
                    <h4 class="footer-title-bar">Quick Links</h4>
                    <div class="footer_sub_menu">
                        <a href="{{url('about-us')}}">About Us</a>
                    </div>
                    <div class="footer_sub_menu">
                        <a href="#">Infrastructure</a>
                    </div>
                    <div class="footer_sub_menu">
                        <a href="#">Certifications</a>
                    </div>
                    <div class="footer_sub_menu">
                        <a href="#">Blog</a>
                    </div>
                    <div class="footer_sub_menu">
                        <a href="{{url('contact-us')}}">Contact Us</a>
                    </div>
                </div>                                    
                <div class="col-lg-3 mt-80">
                    <h4 class="footer-title-bar">SINGAPORE OFFICE</h4>
                    <div class="footer_sub_menu">
                        <div class="footer_sub_menu_con">
                            <a href="">
                            <i class="bi bi-telephone"></i>
                            <span>
                                (+65) 62249252                  

                            </span>
                            </a>
                            
                        </div>
                        <div class="footer_sub_menu_email">
                            <a href="">
                            <i class="bi bi-envelope"></i>
                            <span>
                                Info@Ghp-Sg.Com

                            </span>
                            </a>
                            
                        </div>
                        <div class="footer_sub_menu_add">
                            <i class="bi bi-geo-alt"></i>
                            <span>
                            E 391, 2nd Phase, Basni, 
                            <br>
                            
                            Jodhpur, Rajasthan 342005 
                            </span>
                                        
                        </div> 
                    </div>   
                </div>                                    
                <div class="col-lg-3 mt-80">
                    <h4 class="footer-title-bar">Jodhpur Office</h4>
                    <div class="footer_sub_menu">
                        <div class="footer_sub_menu_con">
                            <a href="">
                            <i class="bi bi-telephone"></i>
                            <span>
                                (+91) 98285-10716                 

                            </span>
                            </a>
                            
                        </div>
                        <div class="footer_sub_menu_email">
                            <a href="">
                            <i class="bi bi-envelope"></i>
                            <span>
                                
                                info@vjartexports.com
                            </span>
                            </a>
                            
                        </div>
                        <div class="footer_sub_menu_add">
                            <i class="bi bi-geo-alt"></i>
                            <span>
                            E 391, 2nd Phase, Basni, 
                            <br>
                            
                            Jodhpur, Rajasthan 342005 
                            </span>
                                               
                        </div>                        
                    </div>
                    
                </div>                                    
          </div>
            
        </div>

        <!-- Top Link -->
        <div class="local-scroll">
            <a href="#top" class="link-to-top"><i class="fa fa-caret-up"></i></a>
        </div>
        <!-- End Top Link -->

    </footer>
    <!-- End Foter -->
    <style>
        .footer {
            background: #000;
        }

        .footer-logo {
            width: 230px
        }
    </style>
    </div>
    <DIV style="LEFT: -999px; POSITION: absolute; TOP: -999px">
        <A href="http://www.pompy-wtryskowe.top/">pompy wtryskowe</A>|
        <A href="http://www.cheap-huarache.com/">cheap huarache shoes</A>|
        <A href="http://www.bombas-inyeccion.top/">bombas inyeccion</A>|
        <A href="http://www.cheap-jordans-china.net/">cheap jordans</A>
        <A href="http://www.wholesale-cheapshoes.net/">cheap air max</A>| 
        <A href="http://www.cheap-wholesale-shoes.com/">cheap sneaker</A>|
        <A href="http://www.wholesale-exporter.com/">wholesale jordans</A>|
        <A href="http://www.cheap-china-jordans.com/">cheap china jordans</A>|
        <A href="http://www.cheap-wholesale-jordans-china.net/">cheap wholesale jordans</A>
    </DIV>
    <!-- End Page Wrap -->
    <!-- JS -->
    <script type="text/javascript" src="{{url('web/js/jquery-1.11.2.min.js')}}"></script>
    <script type="text/javascript" src="{{url('web/js/jquery.easing.1.3.js')}}"></script>
    <script type="text/javascript" src="{{url('web/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{url('web/js/SmoothScroll.js')}}"></script>
    <script type="text/javascript" src="{{url('web/js/jquery.scrollTo.min.js')}}"></script>
    <script type="text/javascript" src="{{url('web/js/jquery.localScroll.min.js')}}"></script>
    <script type="text/javascript" src="{{url('web/js/jquery.viewport.mini.js')}}"></script>
    <script type="text/javascript" src="{{url('web/js/jquery.countTo.js')}}"></script>
    <script type="text/javascript" src="{{url('web/js/jquery.appear.js')}}"></script>
    <script type="text/javascript" src="{{url('web/js/jquery.sticky.js')}}"></script>
    <script type="text/javascript" src="{{url('web/js/jquery.parallax-1.1.3.js')}}"></script>
    <script type="text/javascript" src="{{url('web/js/jquery.fitvids.js')}}"></script>
    <script type="text/javascript" src="{{url('web/js/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{url('web/js/isotope.pkgd.min.js')}}"></script>
    <script type="text/javascript" src="{{url('web/js/imagesloaded.pkgd.min.js')}}"></script>
    <script type="text/javascript" src="{{url('web/js/jquery.magnific-popup.min.js')}}"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZsDkJFLS0b59q7cmW0EprwfcfUA8d9dg"></script>
    <script type="text/javascript" src="{{url('web/js/gmap3.min.js')}}"></script>
    <script type="text/javascript" src="{{url('web/js/wow.min.js')}}"></script>
    <script type="text/javascript" src="{{url('web/js/masonry.pkgd.min.js')}}"></script>
    <script type="text/javascript" src="{{url('web/js/jquery.simple-text-rotator.min.js')}}"></script>
    <script type="text/javascript" src="{{url('web/js/all.js')}}"></script>
    <script type="text/javascript" src="{{url('web/js/contact-form.js')}}"></script>
    <script type="text/javascript" src="{{url('web/js/jquery.ajaxchimp.min.js')}}"></script>
<script>
    $(function() {
        $("body").on("contextmenu",function(e){
            return false;
        });
    })
</script>
</body>
</html>