@extends('frontend.layouts.app')

@section('site_title','Home')

@section('content')

<!-- Head Section -->
<section class="page-section bg-dark-alfa-50 parallax-3" data-background="{{url('web/image/1004/section-bg-10.jpg')}}">
        <div class="relative container align-left">
            <div class="row">
                <div class="col-md-8">
                    <h1 class="hs-line-11 font-alt mb-20 mb-xs-0">About Us</h1>
                    <div class="hs-line-4 font-alt">
                        Extraordinary art team &amp; creative minimalism lovers at VJ
                    </div>
                </div>

                <div class="col-md-4 mt-30">
                    <div class="mod-breadcrumbs font-alt align-right">
                        <a href="{{url('/')}}">Home</a>
                                <span>&nbsp;/ &nbsp;</span><span>About Us</span>
                    </div>

                </div>
            </div>

        </div>
    </section>
<!-- End Head Section -->
<!-- About Section -->
    <section class="page-section" id="about">
        <div class="container relative">

            <div class="section-text mb-50 mb-sm-20">
                <div class="row">
                    <div class="col-md-12">
                        <blockquote>
                            <div>
                                <p style="text-align: center;">
                                    VJ Art Exports is a well-known company operating in the domain of Iron Furniture searches the globe for the finest furnishings available and backs every sale with the best customer service in the industry from last 30 years. The company has own processing unit at Jodhpur, Rajasthan, India. The entire premise operates under the prominent directorship of Mr. Gaurav Jangid, who possesses a unique insight for the business. VJ Art Exports offers up the best in-stock selection at guaranteed best prices and predominantly Exporting in US, Europe, Russia, UK, Africa, Japan, Korea and Australia.
                                </p>
                                <br>
                                <p style="text-align: center;">
                                VJ Art Exports is the place, which creates the exclusive designs with their great technical skills to find any furniture from contemporary to antique. We deal in beautiful modern furniture, carry rare & peculiar designing of Modern contemporary, Vintage Industrial, Straight line, Accent Furniture, and customize products.
                                </p>
                                <br>
                                <p style="text-align: center;">
                                But what makes us different? It’s simple; the products we choose to sell are always selected for their functionality and enduring style. Whether it’s Modern contemporary, Vintage Industrial, Straight line or Accent Furniture, all of our designer brands have one thing in common; their commitment to high quality, innovative and modern design. You’ll be sure to find something that’s right for you. We offer practical stylish and budget friendly furniture to help make our mark. Be rest assured that we are committed to your complete satisfaction.
                                </p>
                            </div>
                        </blockquote>

                    </div>
                </div>
            </div>

                <div class="row">
                        <!-- Team item -->
                        <div class="col-sm-4 mb-xs-30 wow fadeInUp">
                            <div class="team-item">
                                <div class="team-item-image">
                                        <img src="{{url('web/image/1008/team-1.jpg')}}" alt="team-1.jpg" />
                                    <div class="team-item-detail">
                                        <h4 class="font-alt normal">Hello &amp; Welcome!</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit lacus, a iaculis diam.</p>
                                            <div class="team-social-links">
                                                <a rel="noopener" href="#" target="_blank"></a> 
                                                <a rel="noopener" href="#" target="_blank"></a> 
                                                <a rel="noopener" href="#" target="_blank"></a>
                                            </div>
                                    </div>
                                </div>
                                <div class="team-item-descr font-alt">
                                    <div class="team-item-name">
                                        Member 1
                                    </div>
                                    <div class="team-item-role">
                                        Designation 1
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Team item -->
                        <!-- Team item -->
                        <div class="col-sm-4 mb-xs-30 wow fadeInUp">
                            <div class="team-item">
                                <div class="team-item-image">
                                        <img src="{{url('web/image/1008/team-1.jpg')}}" alt="team-1.jpg" />
                                    <div class="team-item-detail">
                                        <h4 class="font-alt normal">Hello &amp; Welcome!</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit lacus, a iaculis diam.</p>
                                        <div class="team-social-links"><a rel="noopener" href="#" target="_blank"></a> 
                                            <a rel="noopener" href="#" target="_blank"></a> 
                                            <a rel="noopener" href="#" target="_blank"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="team-item-descr font-alt">
                                    <div class="team-item-name">
                                        Member 2
                                    </div>
                                    <div class="team-item-role">
                                        Designation 2
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Team item -->
                </div>
        </div>
    </section>
    <!-- End About Section -->
<!-- Testimonials Section -->
    <section class="page-section bg-dark bg-dark-alfa-90 fullwidth-slider" data-background="images/full-width-images/section-bg-3.html">
                <!-- Slide Item -->
                <div>
                    <div class="container relative">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 align-center">
                                <!-- Section Icon -->
                                <div class="section-icon">
                                    <span class="icon-quote"></span>
                                </div>
                                <!-- Section Title --><h3 class="small-title font-alt">What people say?</h3>
                                <blockquote class="testimonial white">
                                    <p>
                                        Phasellus luctus commodo ullamcorper a posuere rhoncus commodo elit. Aenean congue, risus utaliquam dapibus. Thanks!
                                    </p>
                                    <footer class="testimonial-author">
                                        John Doe, doodle inc.
                                    </footer>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Slide Item -->
                <!-- Slide Item -->
                <div>
                    <div class="container relative">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 align-center">
                                <!-- Section Icon -->
                                <div class="section-icon">
                                    <span class="icon-quote"></span>
                                </div>
                                <!-- Section Title --><h3 class="small-title font-alt">What people say?</h3>
                                <blockquote class="testimonial white">
                                    <p>
                                        Phasellus luctus commodo ullamcorper a posuere rhoncus commodo elit. Aenean congue, risus utaliquam dapibus. Thanks!
                                    </p>
                                    <footer class="testimonial-author">
                                        John Doe, doodle inc.
                                    </footer>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Slide Item -->
    </section>
    <!-- End Testimonials Section -->

<!-- Divider -->
<hr class="mt-0 mb-0 " />
<!-- End Divider -->
<!-- Features Section -->
    <section class="page-section">
        <div class="container relative">

            <h2 class="section-title font-alt mb-70 mb-sm-40">
                Why Choose Us?
            </h2>

                <!-- Features Grid -->
                <div class="row multi-columns-row alt-features-grid">
                        <!-- Features Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="alt-features-item align-center">
                                <div class="alt-features-icon">
                                    <span class="icon-flag"></span>
                                </div>
                                <h3 class="alt-features-title font-alt">We’re Creative</h3>
                                <div class="alt-features-descr align-left">
                                    Lorem ipsum dolor sit amet, c-r adipiscing elit. In maximus ligula semper metus pellentesque mattis. Maecenas volutpat, diam enim.
                                </div>
                            </div>
                        </div>
                        <!-- End Features Item -->
                        <!-- Features Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="alt-features-item align-center">
                                <div class="alt-features-icon">
                                    <span class="icon-clock"></span>
                                </div>
                                <h3 class="alt-features-title font-alt">We’re Punctual</h3>
                                <div class="alt-features-descr align-left">
                                    Proin fringilla augue at maximus vestibulum. Nam pulvinar vitae neque et porttitor. Praesent sed nisi eleifend, lorem fermentum orci sit amet, iaculis libero.
                                </div>
                            </div>
                        </div>
                        <!-- End Features Item -->
                        <!-- Features Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="alt-features-item align-center">
                                <div class="alt-features-icon">
                                    <span class="icon-hotairballoon"></span>
                                </div>
                                <h3 class="alt-features-title font-alt">We have magic</h3>
                                <div class="alt-features-descr align-left">
                                    Curabitur iaculis accumsan augue, nec finibus mauris pretium eu. Duis placerat ex gravida nibh tristique porta. Nulla facilisi. Suspendisse ultricies eros blandit.
                                </div>
                            </div>
                        </div>
                        <!-- End Features Item -->
                        <!-- Features Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="alt-features-item align-center">
                                <div class="alt-features-icon">
                                    
                                </div>
                                <h3 class="alt-features-title font-alt">Why Choose Us</h3>
                                <div class="alt-features-descr align-left">
                                    It’s simple; the products we choose to sell are always selected for their functionality and enduring style. Whether it’s Modern contemporary, Vintage Industrial, Straight line or Accent Furniture, all of our designer brands have one thing in common; their commitment to high quality, innovative and modern design. You’ll be sure to find something that’s right for you. We offer practical stylish and budget friendly furniture to help make our mark. Be rest assured that we are committed to your complete satisfaction.
                                </div>
                            </div>
                        </div>
                        <!-- End Features Item -->
                </div>
                <!-- End Features Grid -->
        </div>
    </section>
    <!-- End Features Section -->

@endsection