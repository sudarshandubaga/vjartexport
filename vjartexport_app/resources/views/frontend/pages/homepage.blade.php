@extends('frontend.layouts.app')

@section('site_title','Home')

@section('content')

<!-- Fullwidth Slider -->
<div class="home-section fullwidth-slider-fade bg-dark" id="home">
                <!-- Slide Item -->
                <section class="home-section bg-scroll bg-dark-alfa-50" data-background="{{url('web/image/1021/03.jpg')}}">

                    <div class="container">
                        
                    
                         <div class="js-height-full container">

                            <!-- Hero Content -->
                            <div class="home-content">
                                <div class="home-text">

                                    <h1 class="hs-line-8 no-transp font-alt mb-50 mb-xs-30">
                                        Branding / Design / Development / Photo
                                    </h1>

                                    <h2 class="hs-line-14 font-alt mb-50 mb-xs-30">
                                        Design Studio
                                    </h2>

                                    <div class="local-scroll">
                                        <p><a data-udi="umb://document/e0ed6eeab6e24f0ca115de457ba19f1e" href="{{route('product.index')}}" title="Products" class="btn btn-mod btn-border-w btn-medium btn-round hidden-xs">See More</a>  <span class="hidden-xs"></span></p>
                                    </div>

                                </div>
                            </div>
                            <!-- End Hero Content -->

                        </div>
                    </div>    
                </section>
                <!-- End Slide Item -->
                <!-- Slide Item -->
                <section class="home-section bg-scroll bg-dark-alfa-50" data-background="{{url('web/image/1023/02.jpg')}}">
                    <div class="js-height-full container">

                        <!-- Hero Content -->
                        <div class="home-content">
                            <div class="home-text">

                                <h1 class="hs-line-8 no-transp font-alt mb-50 mb-xs-30">
                                    We Are just crative people
                                </h1>

                                <h2 class="hs-line-14 font-alt mb-50 mb-xs-30">
                                    Creative Studio
                                </h2>

                                <div class="local-scroll">
                                    <p><a data-udi="umb://document/e0ed6eeab6e24f0ca115de457ba19f1e" href="{{route('product.index')}}" title="Products" class="btn btn-mod btn-border-w btn-medium btn-round hidden-xs">See More</a>  <span class="hidden-xs"></span></p>
                                </div>

                            </div>
                        </div>
                        <!-- End Hero Content -->

                    </div>
                </section>
                <!-- End Slide Item -->
                <!-- Slide Item -->
                <section class="home-section bg-scroll bg-dark-alfa-50" data-background="{{url('web/image/1022/01.jpg')}}">
                    <div class="js-height-full container">

                        <!-- Hero Content -->
                        <div class="home-content">
                            <div class="home-text">

                                <h1 class="hs-line-8 no-transp font-alt mb-50 mb-xs-30">
                                    Create your dream with
                                </h1>

                                <h2 class="hs-line-14 font-alt mb-50 mb-xs-30">
                                    Amazing Design
                                </h2>

                                <div class="local-scroll">
                                    <div><a data-udi="umb://document/a015a602497c4ddab7ef0d81518f0a18" href="{{url('contact-us')}}" title="Contact Us" class="btn btn-mod btn-border-w btn-medium btn-round">Get Pricing</a></div>
                                </div>

                            </div>
                        </div>
                        <!-- End Hero Content -->

                    </div>
                </section>
                <!-- End Slide Item -->
                <!-- Slide Item -->
                <section class="home-section bg-scroll bg-dark-alfa-50" data-background="{{url('web/image/1027/ajp_9772.jpg')}}">
                    <div class="js-height-full container">

                        <!-- Hero Content -->
                        <div class="home-content">
                            <div class="home-text">

                                <h1 class="hs-line-8 no-transp font-alt mb-50 mb-xs-30">
                                    
                                </h1>

                                <h2 class="hs-line-14 font-alt mb-50 mb-xs-30">
                                    
                                </h2>

                                <div class="local-scroll">
                                    
                                </div>

                            </div>
                        </div>
                        <!-- End Hero Content -->

                    </div>
                </section>
                <!-- End Slide Item -->
    </div>
    <!-- End Fullwidth Slider -->
<!-- About Section -->
    <section class="page-section" id="about">
        <div class="container relative">

            <h2 class="section-title font-alt align-left mb-70 mb-sm-40">
                About VJ Art Exports

                    <a href="index.html" class="section-more right">More about us <i class="bi bi-chevron-left"></i></a>
            </h2>

            <div class="section-text">
                <div class="row">
                    <div class="col-md-4 mb-sm-50 mb-xs-30"><!-- Bar Item -->
                        <div class="progress tpl-progress">
                            <div class="progress-bar" style="width: 90%;" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">Branding, % <span>90</span>
                            </div>
                        </div>
                        <!-- End Bar Item --> <!-- Bar Item -->
                        <div class="progress tpl-progress">
                        <div class="progress-bar" style="width: 80%;" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100">Design, % <span>80</span></div>
                        </div>
                        <!-- End Bar Item --> <!-- Bar Item -->
                        <div class="progress tpl-progress">
                        <div class="progress-bar" style="width: 85%;" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100">Development, % <span>85</span></div>
                        </div>
                        <!-- End Bar Item --></div>
                        <div class="col-md-4 col-sm-6 mb-sm-50 mb-xs-30">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In maximus ligula semper metus pellentesque mattis. Maecenas volutpat, diam enim sagittis quam, id porta quam. Sed id dolor consectetur fermentum nibh volutpat, accumsan purus.</div>
                        <div class="col-md-4 col-sm-6 mb-sm-50 mb-xs-30">Etiam sit amet fringilla lacus. Pellentesque suscipit ante at ullamcorper pulvinar neque porttitor. Integer lectus. Praesent sed nisi eleifend, fermentum orci amet, iaculis libero. Donec vel ultricies purus. Nam dictum sem, eu aliquam.</div>
                </div>
            </div>

        </div>
    </section>
    <!-- End About Section -->
<!-- Divider -->
<hr class="mt-0 mb-0 " />
<!-- End Divider -->
<!-- Services Section -->
    <section class="page-section" id="services">
        <div class="container relative">

            <h2 class="section-title font-alt mb-70 mb-sm-40">
                Services
            </h2>

                <!-- Nav tabs -->
                <ul class="nav nav-tabs tpl-alt-tabs font-alt pt-30 pt-sm-0 pb-30 pb-sm-0">
                        <li class='active'>

                            <a href="#Branding" data-toggle="tab">

                                <div class="alt-tabs-icon">
                                    <span class="icon-strategy"></span>
                                </div>

                                Branding
                            </a>
                        </li>
                        <li class=''>

                            <a href="#Web-Design" data-toggle="tab">

                                <div class="alt-tabs-icon">
                                    <span class="icon-desktop"></span>
                                </div>

                                Web Design
                            </a>
                        </li>
                </ul>
                <!-- End Nav tabs -->
                <!-- Tab panes -->
                <div class="tab-content tpl-tabs-cont">
                        <!-- Service Item -->
                        <div class='tab-pane fade in active' id="Branding">

                            <div class="section-text">
                                <div class="row">
                                    <div class="col-md-4 mb-md-40 mb-xs-30">
<blockquote class="mb-0">
<p>A brand for a company is like a reputation for a person. You earn reputation by trying to do hard things well.</p>
Jeff Bezos</blockquote>
</div>
<div class="col-md-4 col-sm-6 mb-sm-50 mb-xs-30">Maecenas volutpat, diam enim sagittis quam, id porta quam. Sed id dolor consectetur fermentum volutpat nibh, accumsan purus. Lorem ipsum dolor sit semper amet, consectetur adipiscing elit. In maximus ligula metus pellentesque mattis.</div>
<div class="col-md-4 col-sm-6 mb-sm-50 mb-xs-30">Donec vel ultricies purus. Nam dictum sem, ipsum aliquam . Etiam sit amet fringilla lacus. Pellentesque suscipit ante at ullamcorper pulvinar neque porttitor. Integer lectus. Praesent sed nisi eleifend, fermentum orci amet, iaculis libero.</div>
                                </div>
                            </div>

                        </div>
                        <!-- End Service Item -->
                        <!-- Service Item -->
                        <div class='tab-pane fade' id="Web-Design">

                            <div class="section-text">
                                <div class="row">
                                    <div class="col-md-4 mb-md-40 mb-xs-30">
<blockquote class="mb-0">
<p>It doesn’t matter how many times I have to click, as long as each click is a mindless, unambiguous choice.</p>
Steve Krug</blockquote>
</div>
<div class="col-md-4 col-sm-6 mb-sm-50 mb-xs-30">Cras mi tortor, laoreet id ornare et, accumsan non magna. Maecenas vulputate accumsan velit. Curabitur a nulla ex. Nam a tincidunt ante. Vitae gravida turpis. Vestibulum varius nulla non nulla scelerisque tristique.</div>
<div class="col-md-4 col-sm-6 mb-sm-50 mb-xs-30">Mauris id viverra augue, eu porttitor diam. Praesent faucibus est a interdum elementum. Nam varius at ipsum id dignissim. Nam a tincidunt ante lorem. Pellentesque suscipit ante at ullamcorper pulvinar neque porttitor.</div>
                                </div>
                            </div>

                        </div>
                        <!-- End Service Item -->
                </div>
                <!-- End Tab panes -->
            <div class="align-center">
                    <a href="{{url('about-us')}}" class="section-more font-alt">View All Services <i class="fa fa-angle-right"></i></a>
            </div>

        </div>
    </section>
    <!-- End Services Section -->
<!-- Call Action Section -->
    <section class="page-section pt-0 pb-0 banner-section bg-dark" data-background="{{url('web/image/1024/04.jpg')}}">
        <div class="container relative">
            <div class="row">
                <div class="col-sm-6">
                    <div class="mt-140 mt-lg-80 mb-140 mb-lg-80">
                        <div class="banner-content">
                            <h3 class="banner-heading font-alt">Looking for exclusive designs?</h3>
                            <div class="banner-decription">
                                Crafted by expert designers.
                            </div>
                            <div class="local-scroll">
                                    <a href="{{route('product.index')}}" class="btn btn-mod btn-w btn-medium btn-round">Let's Talk</a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!-- End Call Action Section -->
<!-- Process Section -->
    <section class="page-section">
        <div class="container relative">
            <!-- Features Grid -->
            <div class="row alt-features-grid">
                <!-- Text Item -->
                <div class="col-sm-3">
                    <div class="alt-features-item align-center">
                        <div class="alt-features-descr align-left">
                            <h4 class="mt-0 font-alt">Work Process</h4>
                            Lorem ipsum dolor sit amet, c-r adipiscing elit. In maximus ligula semper metus pellentesque mattis. Maecenas  volutpat, diam enim.
                        </div>
                    </div>
                </div>
                <!-- End Text Item -->
                <div class="col-sm-3">
                    <div class="alt-features-item align-center">
                    <div class="alt-features-icon"><span class="icon-chat"></span></div>
                    <h3 class="alt-features-title font-alt">1. Discuss</h3>
                    </div>
                    </div>
                    <div class="col-sm-3">
                    <div class="alt-features-item align-center">
                    <div class="alt-features-icon"><span class="icon-browser"></span></div>
                    <h3 class="alt-features-title font-alt">2. Make</h3>
                    </div>
                    </div>
                    <div class="col-sm-3">
                    <div class="alt-features-item align-center">
                    <div class="alt-features-icon"><span class="icon-heart"></span></div>
                    <h3 class="alt-features-title font-alt">3. Product</h3>
                    </div>
                    </div>
            </div>
            <!-- End Features Grid -->

        </div>
    </section>
    <!-- End Process Section -->

<!-- Call Action Section -->
    <section class="small-section bg-dark">
        <div class="container relative">
            <div class="align-center">
                <h3 class="banner-heading font-alt">Want to see more works?</h3>
                <div class="local-scroll">
                        <a href="products/index.html" class="btn btn-mod btn-w btn-medium btn-round">Lets view portfolio</a>
                </div>
            </div>
        </div>
    </section>
    <!-- End Call Action Section -->
<!-- Portfolio Section -->
    <section class="page-section pb-0" id="portfolio">
        <div class="relative">

            <h2 class="section-title font-alt mb-70 mb-sm-40">
                Latest Works
            </h2>

            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">

                        <div class="section-text align-center mb-70 mb-xs-40">
                            Curabitur eu adipiscing lacus, a iaculis diam. Nullam placerat blandit auctor. Nulla accumsan ipsum et nibh rhoncus, eget tempus sapien ultricies. Donec mollis lorem vehicula.
                        </div>

                    </div>
                </div>
            </div>

                <!-- Works Grid -->
                <div class="container">
                <ul class="works-grid work-grid-3 work-grid-gut clearfix font-alt hover-white hide-titles" id="work-grid">
                                <!-- Work Item (External Page) -->
                                <li class="work-item">
                                    <a href='products/vj-1002/index.html' class="work-ext-link">
                                        <div class="work-img">
                                            <img class="work-img" src="{{url('web/image/1006/projects-1.jpg')}}" alt="Work" />
                                        </div>
                                        <div class="work-intro">
                                            <h3 class="work-title">Men</h3>
                                            <div class="work-descr">
                                                View More
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- End Work Item -->
                                <!-- Work Item (External Page) -->
                                <li class="work-item">
                                    <a href='index.html' class="work-ext-link">
                                        <div class="work-img">
                                            <img class="work-img" src="{{url('web/image/1006/projects-1.jpg')}}" alt="Work" />
                                        </div>
                                        <div class="work-intro">
                                            <h3 class="work-title">Women</h3>
                                            <div class="work-descr">
                                                View More
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- End Work Item -->
                                <!-- Work Item (External Page) -->
                                <li class="work-item">
                                    <a href='index.html' class="work-ext-link">
                                        <div class="work-img">
                                            <img class="work-img" src="{{url('web/image/1006/projects-1.jpg')}}" alt="Work" />
                                        </div>
                                        <div class="work-intro">
                                            <h3 class="work-title">Man with Beg</h3>
                                            <div class="work-descr">
                                                View More
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- End Work Item -->
                                <!-- Work Item (External Page) -->
                                <li class="work-item">
                                    <a href='index.html' class="work-ext-link">
                                        <div class="work-img">
                                            <img class="work-img" src="{{url('web/image/1044/01.jpg')}}" alt="Work" />
                                        </div>
                                        <div class="work-intro">
                                            <h3 class="work-title"></h3>
                                            <div class="work-descr">
                                                
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- End Work Item -->
                    </ul>
                </div>
                    
                <!-- End Works Grid -->
        </div>
    </section>
    <!-- End Portfolio Section -->

<!-- Features Section -->
    <section class="page-section">
        <div class="container relative">

            <h2 class="section-title font-alt mb-70 mb-sm-40">
                Why Choose Us?
            </h2>

                <!-- Features Grid -->
                <div class="row multi-columns-row alt-features-grid">
                        <!-- Features Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="alt-features-item align-center">
                                <div class="alt-features-icon">
                                    <span class="icon-flag"></span>
                                </div>
                                <h3 class="alt-features-title font-alt">We’re Creative</h3>
                                <div class="alt-features-descr align-left">
                                    Lorem ipsum dolor sit amet, c-r adipiscing elit. In maximus ligula semper metus pellentesque mattis. Maecenas volutpat, diam enim.
                                </div>
                            </div>
                        </div>
                        <!-- End Features Item -->
                        <!-- Features Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="alt-features-item align-center">
                                <div class="alt-features-icon">
                                    <span class="icon-clock"></span>
                                </div>
                                <h3 class="alt-features-title font-alt">We’re Punctual</h3>
                                <div class="alt-features-descr align-left">
                                    Proin fringilla augue at maximus vestibulum. Nam pulvinar vitae neque et porttitor. Praesent sed nisi eleifend, lorem fermentum orci sit amet, iaculis libero.
                                </div>
                            </div>
                        </div>
                        <!-- End Features Item -->
                        <!-- Features Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="alt-features-item align-center">
                                <div class="alt-features-icon">
                                    <span class="icon-hotairballoon"></span>
                                </div>
                                <h3 class="alt-features-title font-alt">We have magic</h3>
                                <div class="alt-features-descr align-left">
                                    Curabitur iaculis accumsan augue, nec finibus mauris pretium eu. Duis placerat ex gravida nibh tristique porta. Nulla facilisi. Suspendisse ultricies eros blandit.
                                </div>
                            </div>
                        </div>
                        <!-- End Features Item -->
                        <!-- Features Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="alt-features-item align-center">
                                <div class="alt-features-icon">
                                    
                                </div>
                                <h3 class="alt-features-title font-alt">Why Choose Us</h3>
                                <div class="alt-features-descr align-left">
                                    It’s simple; the products we choose to sell are always selected for their functionality and enduring style. Whether it’s Modern contemporary, Vintage Industrial, Straight line or Accent Furniture, all of our designer brands have one thing in common; their commitment to high quality, innovative and modern design. You’ll be sure to find something that’s right for you. We offer practical stylish and budget friendly furniture to help make our mark. Be rest assured that we are committed to your complete satisfaction.
                                </div>
                            </div>
                        </div>
                        <!-- End Features Item -->
                </div>
                <!-- End Features Grid -->
        </div>
    </section>
    <!-- End Features Section -->
<!-- Testimonials Section -->
    <section class="page-section bg-dark bg-dark-alfa-90 fullwidth-slider" data-background="images/full-width-images/section-bg-3.html">
                <!-- Slide Item -->
                <div>
                    <div class="container relative">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 align-center">
                                <!-- Section Icon -->
                                <div class="section-icon">
                                    <span class="icon-quote"></span>
                                </div>
                                <!-- Section Title --><h3 class="small-title font-alt">What people say?</h3>
                                <blockquote class="testimonial white">
                                    <p>
                                        Phasellus luctus commodo ullamcorper a posuere rhoncus commodo elit. Aenean congue, risus utaliquam dapibus. Thanks!
                                    </p>
                                    <footer class="testimonial-author">
                                        John Doe, doodle inc.
                                    </footer>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Slide Item -->
                <!-- Slide Item -->
                <div>
                    <div class="container relative">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 align-center">
                                <!-- Section Icon -->
                                <div class="section-icon">
                                    <span class="icon-quote"></span>
                                </div>
                                <!-- Section Title --><h3 class="small-title font-alt">What people say?</h3>
                                <blockquote class="testimonial white">
                                    <p>
                                        Phasellus luctus commodo ullamcorper a posuere rhoncus commodo elit. Aenean congue, risus utaliquam dapibus. Thanks!
                                    </p>
                                    <footer class="testimonial-author">
                                        John Doe, doodle inc.
                                    </footer>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Slide Item -->
    </section>
    <!-- End Testimonials Section -->
<!-- Blog Section -->
    <section class="page-section" id="news">
        <div class="container relative">

            <h2 class="section-title font-alt align-left mb-70 mb-sm-40">
                Latest News

                <a href='index.html' class="section-more right">All News in our blog <i class="fa fa-angle-right"></i></a>
            </h2>
                <div class="row multi-columns-row">
                        <!-- Post Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4 mb-md-50 wow fadeIn" data-wow-delay="0.1s" data-wow-duration="2s">

                            <div class="post-prev-img">
                                <a href='about-us/index.html'>
                                        <img src="{{url('web/image/1007/post-prev-1.jpg')}}" alt="post-prev-1.jpg" />
                                </a>
                            </div>

                            <div class="post-prev-title font-alt">
                                <a href='about-us/index.html'>New Web Design Trends</a>
                            </div>

                            <div class="post-prev-info font-alt">
                                <a href='about-us/index.html'>John Deo</a> | 03 Jun 2018
                            </div>

                            <div class="post-prev-text">
                                Maecenas volutpat, diam enim sagittis quam, id porta quam. Sed id dolor consectetur fermentum nibh volutpat, accumsan purus.
                            </div>

                            <div class="post-prev-more">
                                <a href='about-us/index.html' class="btn btn-mod btn-gray btn-round">Read More <i class="fa fa-angle-right"></i></a>
                            </div>

                        </div>
                        <!-- End Post Item -->
                        <!-- Post Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4 mb-md-50 wow fadeIn" data-wow-delay="0.1s" data-wow-duration="2s">

                            <div class="post-prev-img">
                                <a href='about-us/index.html'>
                                        <img src="{{url('web/image/1007/post-prev-1.jpg')}}" alt="post-prev-1.jpg" />
                                </a>
                            </div>

                            <div class="post-prev-title font-alt">
                                <a href='about-us/index.html'>Minimalistic Design Forever</a>
                            </div>

                            <div class="post-prev-info font-alt">
                                <a href='about-us/index.html'>John Deo</a> | 24 May 2018
                            </div>

                            <div class="post-prev-text">
                                Maecenas volutpat, diam enim sagittis quam, id porta quam. Sed id dolor consectetur fermentum nibh volutpat, accumsan purus.
                            </div>

                            <div class="post-prev-more">
                                <a href='about-us/index.html' class="btn btn-mod btn-gray btn-round">Red More <i class="fa fa-angle-right"></i></a>
                            </div>

                        </div>
                        <!-- End Post Item -->
                </div>
        </div>
    </section>
    <!-- End Blog Section -->
<!-- Divider -->
<hr class="mt-0 mb-0" />
<!-- End Divider -->
<!-- Contact Section -->
    <section class="page-section" id="contact">
        <div class="container relative">

            <h2 class="section-title font-alt mb-70 mb-sm-40">
                Find Us
            </h2>

                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="row">
                                <div class="col-sm-6 col-lg-4 pt-20 pb-20 pb-xs-0">
                                    <div class="contact-item">
                                        <div class="ci-icon">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <div class="ci-title font-alt">
                                            Call Us
                                        </div>
                                        <div class="ci-text">
                                            +61 3 8376 6284 
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-4 pt-20 pb-20 pb-xs-0">
                                    <div class="contact-item">
                                        <div class="ci-icon">
                                            <i class="fa fa-map-marker"></i>
                                        </div>
                                        <div class="ci-title font-alt">
                                            Address
                                        </div>
                                        <div class="ci-text">
                                            245 Quigley Blvd, Ste K 
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-4 pt-20 pb-20 pb-xs-0">
                                    <div class="contact-item">
                                        <div class="ci-icon">
                                            <i class="fa fa-envelope"></i>
                                        </div>
                                        <div class="ci-title font-alt">
                                            Email 
                                        </div>
                                        <div class="ci-text">
                                            info@vjartexports.com
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>

                </div>
        </div>
    </section>
    <!-- End Contact Section -->

@endsection