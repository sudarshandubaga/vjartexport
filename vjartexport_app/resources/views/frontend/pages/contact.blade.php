@extends('frontend.layouts.app')

@section('site_title','Home')

@section('content')

<!-- Head Section -->
<section class="page-section bg-dark-alfa-50 parallax-3" data-background="{{url('web/image/1004/section-bg-10.jpg')}}">
        <div class="relative container align-left">
            <div class="row">
                <div class="col-md-8">
                    <h1 class="hs-line-11 font-alt mb-20 mb-xs-0">Contact Us</h1>
                    <div class="hs-line-4 font-alt">
                        We’re always open to talk to good people 
                    </div>
                </div>

                <div class="col-md-4 mt-30">
                    <div class="mod-breadcrumbs font-alt align-right">
                        <a href="/">Home</a>
                                <span>&nbsp;/ &nbsp;</span><span>Contact Us</span>
                    </div>

                </div>
            </div>

        </div>
    </section>
    <!-- End Head Section -->

    <!-- Contact Section -->
    <section class="page-section" id="contact">
        <div class="container relative">

            <h2 class="section-title font-alt mb-70 mb-sm-40">
                Have a questions?
            </h2>

            <div class="row mb-60 mb-xs-40">

                <div class="col-md-8 col-md-offset-2">
                    <div class="row">
                            <!-- Phone -->
                            <div class="col-sm-6 col-lg-4 pt-20 pb-20 pb-xs-0">
                                <div class="contact-item">
                                    <div class="ci-icon">
                                        <i class="bi bi-telephone"></i>
                                    </div>
                                    <div class="ci-title font-alt">
                                        Call Us
                                    </div>
                                    <div class="ci-text">+91 9829441476</div>
                                </div>
                            </div>
                            <!-- End Phone -->
                                                    <!-- Address -->
                            <div class="col-sm-6 col-lg-4 pt-20 pb-20 pb-xs-0">
                                <div class="contact-item">
                                    <div class="ci-icon">
                                        <i class="bi bi-geo-alt"></i>
                                    </div>
                                    <div class="ci-title font-alt">
                                        Address
                                    </div>
                                    <div class="ci-text">E 391, 2nd Phase, Basni, Jodhpur, Rajasthan 342005</div>
                                </div>
                            </div>
                            <!-- End Address -->
                                                    <!-- Email -->
                            <div class="col-sm-6 col-lg-4 pt-20 pb-20 pb-xs-0">
                                <div class="contact-item">
                                    <div class="ci-icon">
                                        <i class="bi bi-envelope"></i>
                                    </div>
                                    <div class="ci-title font-alt">
                                        Email
                                    </div>
                                    <div class="ci-text"><a href="mailto:info@vjartexports.com">info@vjartexports.com</a></div>
                                </div>
                            </div>
                            <!-- End Email -->
                    </div>
                </div>

            </div>

            <!-- Contact Form -->
            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                    <form class="form contact-form" id="contact_form">
                        <div class="clearfix">

                            <div class="cf-left-col">

                                <!-- Name -->
                                <div class="form-group">
                                    <input type="text" name="name" id="name" class="input-md round form-control" placeholder="Name" pattern=".{3,100}" required>
                                </div>

                                <!-- Email -->
                                <div class="form-group">
                                    <input type="email" name="email" id="email" class="input-md round form-control" placeholder="Email" pattern=".{5,100}" required>
                                </div>

                            </div>

                            <div class="cf-right-col">

                                <!-- Message -->
                                <div class="form-group">
                                    <textarea name="message" id="message" class="input-md round form-control" style="height: 84px;" placeholder="Message"></textarea>
                                </div>

                            </div>

                        </div>

                        <div class="clearfix">

                            <div class="cf-left-col">

                                <!-- Inform Tip -->
                                <div class="form-tip pt-20">
                                    <i class="bi bi-info-circle"></i> All the fields are required
                                </div>

                            </div>

                            <div class="cf-right-col">

                                <!-- Send Button -->
                                <div class="align-right pt-10">
                                    <button class="submit_btn btn btn-mod btn-medium btn-round" id="submit_btn">Submit Message</button>
                                </div>

                            </div>

                        </div>



                        <div id="result"></div>
                    </form>

                </div>
            </div>
            <!-- End Contact Form -->

        </div>
    </section>
    <!-- End Contact Section -->
        <!-- Google Map -->
        <div class="google-map">

            <div>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d116477.95926329125!2d72.95565969718093!3d26.263858538806453!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39418bff6010b135%3A0x7d006f1050d4735!2sVJ%20Art%20Exports!5e1!3m2!1sen!2sin!4v1645428626381!5m2!1sen!2sin" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>

            <div class="map-section">

                <div class="map-toggle">
                    <div class="mt-icon">
                        <i class="fa fa-map-marker"></i>
                    </div>
                    <div class="mt-text font-alt">
                        <div class="mt-open">Open the map <i class="fa fa-angle-down"></i></div>
                        <div class="mt-close">Close the map <i class="fa fa-angle-up"></i></div>
                    </div>
                </div>

            </div>

        </div>
        <!-- End Google Map -->

@endsection        